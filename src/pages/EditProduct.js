import React, { Fragment, useEffect, useState } from 'react';
import EditProducts from '../components/EditProducts/index';
import { withRouter } from 'react-router-dom';

function EditProduct(props) {
    // const {name, description, price} = props.location.params;
    const name = props.location.params && props.location.params.name !== undefined ? props.location.params.name : '';
    const description = props.location.params && props.location.params.description !== undefined ? props.location.params.description : '';
    const price = props.location.params && props.location.params.price !== undefined ? props.location.params.price : '';
    const id = props.match.params.productId;
    return (
        <Fragment>
            <EditProducts id={id} name={name} description={description} price={price} />
        </Fragment>
    );
}

export default (withRouter(EditProduct));