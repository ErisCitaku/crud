import React, { Fragment } from 'react';
import ViewProducts from '../components/ViewProducts/index';
import { withRouter } from 'react-router-dom'

function ViewProduct(props) {
    const image = props.location.params && props.location.params.image !== undefined ? props.location.params.image : '';
    const name = props.location.params && props.location.params.name !== undefined ? props.location.params.name : '';
    const description = props.location.params && props.location.params.description !== undefined ? props.location.params.description : '';
    const price = props.location.params && props.location.params.price !== undefined ? props.location.params.price : '';
    const id = props.match.params.productId;
    return (
        <Fragment>
            <ViewProducts id={id} name={name} description={description} price={price} image={image} />
        </Fragment>
    );
}

export default (withRouter(ViewProduct));