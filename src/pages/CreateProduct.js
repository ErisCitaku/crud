import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import CreateProducts from '../components/CreateProducts/index';

function CreateProduct(props) {
    return (
        <Fragment>
            <CreateProducts />
        </Fragment>
    );
}

export default (withRouter(CreateProduct));