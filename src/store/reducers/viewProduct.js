export default function (state = {}, action) {
    const { data, type } = action;
    if (type === 'VIEW_PRODUCT') {
        return data;
    } else {
        return state;
    }
}