export default function (state = false, action) {
    const { data, type } = action;
    if (type === 'DELETE_PRODUCTS') {
        return !state;
    } else {
        return state;
    }
}