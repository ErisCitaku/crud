export default function (state = [], action){
    const {data, type} = action;
    if(type === 'UPDATE_PRODUCTS'){
        return data;
    }else{
        return state;
    }
}