export default function (state = [], action) {
    const { data, type } = action;

    if (type === 'LIST_ALL_PRODUCTS') {
        return data;
    } else {
        return state;
    }
} 