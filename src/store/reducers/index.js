import { combineReducers } from 'redux';
import viewAllProducts from './listAllProducts';
import deleteProducts from './deleteProducts';
import updateProducts from './updateProducts';
import createProducts from './createProducts';
import viewProduct from './viewProduct';


export default combineReducers({
    viewAllProducts,
    deleteProducts,
    updateProducts,
    createProducts,
    viewProduct,
});