import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';
import reduxThunk from 'redux-thunk';

export default createStore(
    reducers,
    compose(
        applyMiddleware(reduxThunk)
    )
)