import axios from 'axios';
import {consumerKey, consumerSecret} from './keys';
export const DeleteProducts = (id) => async (dispatch) => {
    try {

        const { status, data } = await axios.delete(`/wp-json/wc/v3/products/${id}?consumer_key=${consumerKey}&consumer_secret=${consumerSecret}`);

        if (status === 200) {
            dispatch({ type: 'DELETE_PRODUCTS', data })
        };

    } catch (e) {
        Promise.reject(e);
    }
}