import axios from 'axios';
import { consumerKey, consumerSecret } from './keys'

export const ViewProduct = (id) => async (dispatch) => {
    try {
        const { status, data } = await axios.get(`/wp-json/wc/v3/products/${id}?consumer_key=${consumerKey}&consumer_secret=${consumerSecret}`);

        if (status === 200) {
            dispatch({ type: 'VIEW_PRODUCT', data });
        }
    } catch (e) {
        Promise.reject(e);
    }
}