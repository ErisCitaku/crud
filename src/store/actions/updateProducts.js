import axios from 'axios';
import { consumerKey, consumerSecret } from './keys';

export const UpdateProducts = (id, name, regular_price, description) => async (dispatch) => {
    try {
        const body = { name, regular_price, description };
        console.log('body', body)
        const { status, data } = await axios.put(`/wp-json/wc/v3/products/${id}?consumer_key=${consumerKey}&consumer_secret=${consumerSecret}`, body)
        if (status === 200) {
            dispatch({ type: 'UPDATE_PRODUCTS', data })
            document.getElementById('spinner-edit').style.display = "none";
        }
    } catch (e) {
        console.log('error', e.message)
        Promise.reject(e);
    }
}