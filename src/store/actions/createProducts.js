import axios from 'axios';
import { consumerKey, consumerSecret } from './keys';

export const CreateProductss = (name, description, regular_price) => async (dispatch) => {
    try {
        const body = { name, description, regular_price };
        const { status, data } = await axios.post(`/wp-json/wc/v3/products/?consumer_key=${consumerKey}&consumer_secret=${consumerSecret}`, body);
        if (status === 200) {
            dispatch({ type: 'CREATE_PRODUCTS', data })
            document.getElementById('spinner-border').style.display = "none";
        }

    } catch (e) {
        Promise.reject(e);
    }
}