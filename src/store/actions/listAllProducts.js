import axios from 'axios';

const consumerKey = 'ck_aca9a6a04fcda6f51674a745e171f66fde1c4a1e';
const consumerSecret = 'cs_459f997fe88e7d29a5b117902b9cd94b20009de8';

export const ListAllProducts = () => async (dispatch) => {
    try {
        const { status, data } = await axios.get(`/wp-json/wc/v3/products?consumer_key=${consumerKey}&consumer_secret=${consumerSecret}`);
        if (status === 200) dispatch({ type: 'LIST_ALL_PRODUCTS', data });
    } catch (e) {
        Promise.reject(e);
    }
}