import React, { Fragment } from 'react';
import './App.css';
import config from './config';
import axios from 'axios';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import history from './history';

// Pages
import HomePage from './pages/HomePage';
import EditProduct from './pages/EditProduct';
import CreateProduct from './pages/CreateProduct';
import ViewProduct from './pages/ViewProduct';

function App() {
  axios.defaults.baseURL = config.baseURL;
  axios.defaults.headers.common = { "Content-Type": "appliciation/json" }
  return (
    <Fragment>
      <Router history={history}>
        <Route exact path='/' component={HomePage} />
        <Route path='/product/edit/:productId/' component={EditProduct} />
        <Route path='/product/add' component={CreateProduct} />
        <Route path='/product/view/:viewProductId' component={ViewProduct} />
      </Router>
    </Fragment>
  );
}

export default App;
