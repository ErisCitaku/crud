import React, { Fragment, useState, useEffect } from 'react';
import './style/style.css'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { CreateProductss } from '../../store/actions/createProducts';

function CreateProducts(props) {

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const formSubmit = (e) => {
        e.preventDefault();
        props.CreateProductss(name, description, price);
        document.getElementById('spinner-border').style.display = "inline-block";
        setTimeout(() => {
            props.history.push('/');
        }, 4000)
    }
    const onNameChange = (e) => {
        setName(e.target.value);
    }
    const onDescriptionChange = (e) => {
        setDescription(e.target.value);
    }
    const onPriceChange = (e) => {
        setPrice(e.target.value);
    }

    return (
        <Fragment>
            <div className="create-product">
                <h1>Create new product</h1>
                <form onSubmit={formSubmit}>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            value={name} id="exampleInputEmail1" placeholder="Enter name" name="name"
                            onChange={onNameChange} />
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="4"
                            value={description} name="description" placeholder="Enter description"
                            onChange={onDescriptionChange}></textarea>
                    </div>
                    <div className="form-group">
                        <label>Price</label>
                        <input type="text" className="form-control"
                            value={price} id="exampleInputEmail1" placeholder="Enter Price" name="regular_price"
                            onChange={onPriceChange} />
                    </div>
                    <button type="submit" className="btn btn-primary" onSubmit={formSubmit}>Submit</button>
                    <div className="spinner-border text-success" id="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </form>
            </div>
        </Fragment>
    );
}

const mapStateToProps = ({ createProducts }) => ({ createProducts });
const mapDispatchToProps = ({ CreateProductss })

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CreateProducts));