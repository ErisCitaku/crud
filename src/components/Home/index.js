import React, { Fragment, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { ListAllProducts } from '../../store/actions/listAllProducts';
import { DeleteProducts } from '../../store/actions/deleteProducts';
import { UpdateProducts } from '../../store/actions/updateProducts';
import { CreateProductss } from '../../store/actions/createProducts';
import './style/style.css';

function Home(props) {

  function handleUpdate(id, name, description, price) {
    props.history.push({
      pathname: '/product/edit/' + id,
      params: { name: name, description: description, price: price }
    })
  }

  function handleView(id, name, description, price, image) {
    props.history.push({
      pathname: '/product/view/' + id,
      params: { name: name, description: description, price: price, image: image }
    })
  }

  function handleCreate() {
    props.history.push(`/product/add`);
  }

  function handleDelete(id) {
    props.DeleteProducts(id);
  }


  useEffect(() => { props.ListAllProducts(); }, [])
  useEffect(() => { props.ListAllProducts(); }, [props.deleteProducts]);
  useEffect(() => { props.ListAllProducts(); }, [props.updateProducts]);
  useEffect(() => { props.ListAllProducts(); }, [props.createProducts]);


  return (
    <Fragment>
      <div className="home">
        <h1>Products</h1>
        <div className="home-product-cards">
          {props.viewAllProducts.map(products => (<div className="card home-card_margin" style={{ width: "18rem" }} key={products.id}>
            <img className="card-img-top card-img-top-height" src={products.images.length && products.images[0].src} alt="Card image cap" />
            <div className="card-body">
              <h5 className="card-title">{products.name}</h5>
              <p className="card-text">{products.price}$</p>
              <a className="btn btn-primary home-card-button_margin" onClick={() => handleView(products.id, products.name, products.description, products.price, products.images.length && products.images[0].src)}>View</a>
              <a className="btn btn-primary home-card-button_margin" onClick={() => handleUpdate(products.id, products.name, products.description, products.price)}>Edit</a>
              <a className="btn btn-primary home-card-button_margin" onClick={() => handleDelete(products.id)}>Delete</a>
            </div>
          </div>))}
        </div>
        <a className="btn btn-primary home-card-button_width" onClick={handleCreate}>Add Product</a>
      </div>
    </Fragment>
  );
}


const mapStateToProps = ({ createProducts, viewAllProducts, deleteProducts, updateProducts }) => ({ viewAllProducts, deleteProducts, updateProducts, createProducts });
const mapDispatchToProps = { ListAllProducts, DeleteProducts, UpdateProducts, CreateProductss };

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));