import React, { Fragment, useState } from 'react';
import './style/style.css';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

function ViewProducts(props) {
    function getBack() {
        props.history.push('/');
    }
    return (
        <Fragment>
            <div className="view-products">
                <h1>View product details</h1>
                <div className="card mb-3">
                    <img className="card-img-top card-img-height" src={props.image} alt="Card image cap" />
                    <div className="card-body">
                        <h5 className="card-title">{props.name}</h5>
                        <p className="card-text">{props.description}</p>
                        <p className="card-text"><small className="text-muted">{props.price}$</small></p>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" onClick = {getBack}>Back</button>
            </div>
        </Fragment>
    )
}

const mapStateToProps = null;
const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ViewProducts));