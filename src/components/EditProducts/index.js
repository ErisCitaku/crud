import React, { Fragment, useEffect, useState } from 'react';
import './style/style.css'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { UpdateProducts } from '../../store/actions/updateProducts';

function EditProducts(props) {

    const [name, setName] = useState(props.name ? props.name : '');
    const [description, setDescription] = useState(props.description ? props.description : '');
    const [price, setPrice] = useState(props.price ? props.price : '');
    const [id, setId] = useState(props.id ? props.id : '')

    const formSubmit = (e) => {
        e.preventDefault();
        document.getElementById('spinner-edit').style.display = "inline-block";
        props.UpdateProducts(id, name, price, description);
        setTimeout(() => {
            props.history.push('/');
        }, 3000)
    }
    const onNameChange = (e) => {
        setName(e.target.value);
    }
    const onDescriptionChange = (e) => {
        setDescription(e.target.value);
    }
    const onPriceChange = (e) => {
        setPrice(e.target.value);
    }
    return (
        <Fragment>
            <div className="edit-products">
                <h1>Edit product</h1>
                <form onSubmit={formSubmit}>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            value={name} id="exampleInputEmail1" placeholder="Enter name" name="name"
                            onChange={onNameChange} />
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="4"
                            value={description} name="description" placeholder="Enter description"
                            onChange={onDescriptionChange}></textarea>
                    </div>
                    <div className="form-group">
                        <label>Price</label>
                        <input type="text" className="form-control"
                            value={price} id="exampleInputEmail1" placeholder="Enter Price" name="regular_price"
                            onChange={onPriceChange} />
                    </div>
                    <button type="submit" className="btn btn-primary" onSubmit={formSubmit}>Save</button>
                    <div className="spinner-border text-primary" id = "spinner-edit" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </form>
            </div>
        </Fragment>
    );
}

const mapStateToProps = ({ updateProducts }) => ({ updateProducts });
const mapDispatchToProps = { UpdateProducts };

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditProducts));